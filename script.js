// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку
// відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована.
// У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть
// додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті,
// через такі правки не переставала працювати.


const tabsNavigation = document.querySelector(".tabs");
const tabContent = document.querySelector(".tabs-content");

const tabsAll = document.querySelectorAll(".tabs-title");
const textContentAll = document.querySelectorAll(".text");
// console.log(tabsNavigation);
// console.log(tabContent);
// console.log(tabsAll);
// console.log(textContentAll);

tabsNavigation.addEventListener("click", (event) => {
//   console.log(event.target);

  textContentAll.forEach((e) => {
    e.classList.remove("active-text");
    e.classList.add("no-active-text");
  });

  tabsAll.forEach((e) => {
    e.classList.remove("active");
  });
  event.target.classList.toggle("active");

  const textContent = tabContent.querySelector(
    `[data-name = ${event.target.dataset.name}]`
  );
//   console.log(textContent);
  
  textContent.classList.remove("no-active-text");
  textContent.classList.add("active-text");
});
